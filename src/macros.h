/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef LIBBST_MACROS_H
# define LIBBST_MACROS_H


#define HAS_PARENT(n) (NULL != (n)->parent)
#define HAS_LEFT(n) (NULL != (n)->left)
#define HAS_RIGHT(n) (NULL != (n)->right)
#define IS_LEFT(n) (HAS_PARENT(n) && (n) == (n)->parent->left)
#define IS_RIGHT(n) (HAS_PARENT(n) && (n) == (n)->parent->right)
#define IS_INNER(n) (HAS_LEFT(n) && HAS_RIGHT(n))
#define IS_LEAF(n) (!(HAS_LEFT(n) || HAS_RIGHT(n)))


#endif
