/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "splay.h"

#include "bst.h"
#include "macros.h"
#include "rotations.h"


#define ZIG(n) { bst_node_rotate_right(n); }
#define ZAG(n) { bst_node_rotate_left(n); }
#define ZIGZIG(n) { ZIG((n)->parent); ZIG(n); }
#define ZAGZAG(n) { ZAG((n)->parent); ZAG(n); }
#define ZIGZAG(n) { ZIG(n); ZAG(n); }
#define ZAGZIG(n) { ZAG(n); ZIG(n); }


static
void
_splay_assign_overrides(struct bst *t);

static
void
_splay(struct bst_node *node, struct bst_node **root);

static
void
_splay_post_insert(struct bst *t, struct bst_node *inserted_node);

static
void
_splay_post_find(struct bst *t, struct bst_node *found, int exact_match);

static
void
_splay_post_remove(struct bst *t, struct bst_node *removed,
                   struct bst_node *replacement, int was_left);


int
splay_init(struct bst *t, struct bst_comparator const *comparator)
{
  int ret;


  ret = bst_init(t, comparator);
  if (ret)
  {
    return ret;
  }

  _splay_assign_overrides(t);

  return 0;
}

int
splay_inita(struct bst *t, struct bst_comparator const *comparator,
            struct bst_allocator const *allocator)
{
  int ret;


  ret = bst_inita(t, comparator, allocator);
  if (ret)
  {
    return ret;
  }

  _splay_assign_overrides(t);

  return 0;
}


/*******************
 * local functions *
 *******************/

void
_splay_assign_overrides(struct bst *t)
{
  t->overrides.post_insert = &_splay_post_insert;
  t->overrides.post_find = &_splay_post_find;
  t->overrides.post_remove = &_splay_post_remove;
}

void
_splay(struct bst_node *node, struct bst_node **root)
{
  while (HAS_PARENT(node))
  {
    if (IS_LEFT(node))
    {
      if (!HAS_PARENT(node->parent))
      {
        ZIG(node);
      }
      else if (IS_LEFT(node->parent))
      {
        ZIGZIG(node);
      }
      else
      {
        ZIGZAG(node);
      }
    }
    else
    {
      if (!HAS_PARENT(node->parent))
      {
        ZAG(node);
      }
      else if (IS_RIGHT(node->parent))
      {
        ZAGZAG(node);
      }
      else
      {
        ZAGZIG(node);
      }
    }
  }

  *root = node;
}

void
_splay_post_insert(struct bst *t, struct bst_node *inserted_node)
{
  _splay(inserted_node, &t->root);
}

void
_splay_post_find(struct bst *t, struct bst_node *found, int exact_match)
{
  (void) exact_match;

  _splay(found, &t->root);
}

void
_splay_post_remove(struct bst *t, struct bst_node *removed,
                   struct bst_node *replacement, int was_left)
{
  (void) replacement;
  (void) was_left;

  if (HAS_PARENT(removed))
  {
    _splay(removed->parent, &t->root);
  }
}
