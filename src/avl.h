/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef LIBBST_AVL_H
# define LIBBST_AVL_H


# include "bst.h"


# ifdef __cplusplus
extern "C"
{
# endif


enum
avl_weight
{
  AVL_WEIGHT_LEFT_HEAVY,
  AVL_WEIGHT_NEUTRAL,
  AVL_WEIGHT_RIGHT_HEAVY
};

struct
avl_node
{
  struct bst_node
  bst_node;

  enum avl_weight
  weight;
};


int
avl_init(struct bst *t, struct bst_comparator const *comparator);

int
avl_inita(struct bst *t, struct bst_comparator const *comparator,
          struct bst_allocator const *allocator);


# ifdef __cplusplus
}
# endif


#endif
