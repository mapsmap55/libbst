/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <bst.h>
#include <splay.h>
#include <avl.h>


#define ESC ((char) 0x1B)


enum
operation
{
  OP_INSERT,
  OP_FIND,
  OP_CONTAINS,
  OP_REMOVE
};

enum
bst_type
{
  BST_TYPE_BST,
  BST_TYPE_SPLAY,
  BST_TYPE_AVL
};


static
int
_strcmp_wrapper(void const *l, void const *r, void *arg);

static
int
_strchoice(char const *str, int choice_count, ...);

static
void
_print_structure(struct bst_node *node, int indent, enum bst_type type);

static
int
_print(void *datum, void *arg);

static
#ifdef __GNUC__
__attribute__ ((pure))
#endif
int
_height(struct bst_node *root);


int
main(int argc, char **argv)
{
  struct bst t;
  int i, tmp, start;
  char *datum;
  enum operation op;
  enum bst_type type;


  printf("libbst_version is %s\n", libbst_version);

  t.comparator.compare = &_strcmp_wrapper;
  t.comparator.arg = NULL;

  if (1 == argc)
  {
    return 0;
  }

  tmp = _strchoice(argv[1], 3, "-bst", "-splay", "-avl");
  start = (-1 == tmp) ? 1 : 2;
  switch (tmp)
  {
    default:
    case -1:
    case 0:
      printf("using a plain BST\n");
      tmp = bst_init(&t, &t.comparator);
      type = BST_TYPE_BST;
      break;
    case 1:
      printf("using a splay tree\n");
      tmp = splay_init(&t, &t.comparator);
      type = BST_TYPE_SPLAY;
      break;
    case 2:
      printf("using an AVL tree\n");
      tmp = avl_init(&t, &t.comparator);
      type = BST_TYPE_AVL;
      break;
  }

  if (tmp)
  {
    fprintf(stderr, "init failed\n");

    return 1;
  }

  op = OP_INSERT;
  for (i = start; i < argc; ++i)
  {
    tmp = _strchoice(argv[i], 11, "-i", "-f", "-c", "-r", "-pinfo", "-ps",
                     "-tpre", "-tin", "-tpost", "-tlevel", "-tlevelf");
    if (-1 != tmp)
    {
      switch (tmp)
      {
        case 0:
          op = OP_INSERT;
          break;
        case 1:
          op = OP_FIND;
          break;
        case 2:
          op = OP_CONTAINS;
          break;
        case 3:
          op = OP_REMOVE;
          break;
        case 4:
          printf("count = %zu\n", t.count);
          printf("height = %zu\n", bst_height(&t));
          break;
        case 5:
          _print_structure(t.root, 0, type);
          break;
        case 6:
          tmp = 1;
          bst_preorder(&t, &_print, &tmp);
          puts("");
          break;
        case 7:
          tmp = 1;
          bst_inorder(&t, &_print, &tmp);
          puts("");
          break;
        case 8:
          tmp = 1;
          bst_postorder(&t, &_print, &tmp);
          puts("");
          break;
        case 9:
          tmp = 1;
          bst_levelorder(&t, &_print, &tmp);
          puts("");
          break;
        case 10:
          tmp = 1;
          bst_levelorder_fast(&t, &_print, &tmp);
          puts("");
          break;
      }
    }
    else
    {
      switch (op)
      {
        case OP_INSERT:
          tmp = bst_insert(&t, argv[i]);
          printf("insert(\"%s\") -> %d\n", argv[i], tmp);
          break;
        case OP_FIND:
          tmp = bst_find(&t, argv[i], (void **) &datum);
          printf("find(\"%s\") -> %d\n", argv[i], tmp);
          if (!tmp)
          {
            printf("  found \"%s\"\n", datum);
          }
          break;
        case OP_CONTAINS:
          tmp = bst_contains(&t, argv[i]);
          printf("contains(\"%s\") -> %d\n", argv[i], tmp);
          break;
        case OP_REMOVE:
          tmp = bst_remove(&t, argv[i], (void **) &datum);
          printf("remove(\"%s\") -> %d\n", argv[i], tmp);
          if (!tmp)
          {
            printf("  removed \"%s\"\n", datum);
          }
          break;
      }
    }
  }

  bst_destroy(&t, NULL, NULL);

  return 0;
}


/*******************
 * local functions *
 *******************/

int
_strcmp_wrapper(void const *l, void const *r, void *arg)
{
  (void) arg;

  return strcmp((char const *) l, (char const *) r);
}

int
_strchoice(char const *str, int choice_count, ...)
{
  va_list choices;
  int i;
  char const *choice;


  va_start(choices, choice_count);
  for (i = 0; i < choice_count; ++i)
  {
    choice = va_arg(choices, char const *);
    if (0 == strcmp(str, choice))
    {
      va_end(choices);

      return i;
    }
  }

  va_end(choices);

  return -1;
}

void
_print_structure(struct bst_node *node, int indent, enum bst_type type)
{
  int i, l, r, n;
  char *extra;


  for (i = 0; i < indent; ++i)
  {
    printf("  ");
  }

  if (NULL == node)
  {
    printf(">\n");

    return;
  }

  switch (type)
  {
    case BST_TYPE_AVL:
      switch (((struct avl_node *) node)->weight)
      {
        case AVL_WEIGHT_LEFT_HEAVY:
          extra = "L";
          n = -1;
          break;
        case AVL_WEIGHT_NEUTRAL:
          extra = "N";
          n = 0;
          break;
        case AVL_WEIGHT_RIGHT_HEAVY:
          extra = "R";
          n = 1;
          break;
        default:
          extra = "?";
          n = 0;
          break;
      }
      l = _height(node->left);
      r = _height(node->right);
      if (n == r - l)
      {
        printf(">\"%s\" (w=%s, h=%d, %d=%d-%d)\n", (char *) node->datum, extra,
               _height(node), n, r, l);
      }
      else
      {
        printf(">\"%s\" (w=%s, h=%d, %c[1;31m%d%c[m=%d-%d)\n",
               (char *) node->datum, extra, _height(node), ESC, n, ESC, r, l);
      }
      break;
    default:
      printf(">\"%s\"\n", (char *) node->datum);
      break;
  }

  if (NULL != node->left || NULL != node->right)
  {
    _print_structure(node->left, indent + 1, type);
    _print_structure(node->right, indent + 1, type);
  }
}

int
_print(void *datum, void *arg)
{
  int *first;


  first = arg;

  if (*first)
  {
    printf("\"%s\"", (char *) datum);
    *first = 0;
  }
  else
  {
    printf(" \"%s\"", (char *) datum);
  }

  return 0;
}

int
_height(struct bst_node *root)
{
  int l, r;


  if (NULL == root)
  {
    return 0;
  }

  l = _height(root->left);
  r = _height(root->right);

  return 1 + ((l > r) ? l : r);
}
