/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef LIBBST_BST_H
# define LIBBST_BST_H


# include <stddef.h>


# ifdef __cplusplus
extern "C"
{
# endif


struct
bst_node
{
  void *
  datum;

  struct bst_node *
  parent;

  struct bst_node *
  left;

  struct bst_node *
  right;
};

struct
bst_comparator
{
  int
  (*compare)(void const *, void const *, void *);

  void *
  arg;
};

struct
bst_allocator
{
  void *
  (*allocate)(size_t, void *);

  void
  (*deallocate)(size_t, void *, void *);

  void *
  arg;
};

struct
bst;

struct
bst_overrides
{
  struct bst_node *
  (*new_node)(struct bst *);

  void
  (*delete_node)(struct bst *, struct bst_node *);

  /* may be NULL; called as (*p_i)(t, inserted_node) */
  /* t and inserted_node are never NULL */
  void
  (*post_insert)(struct bst *, struct bst_node *);

  /* may be NULL; called as (*p_f)(t, found, exact_match) */
  /* t and found are never NULL */
  void
  (*post_find)(struct bst *, struct bst_node *, int);

  /* may be NULL; called as (*p_r)(t, removed, replacement, was_left) */
  /* t and removed are never NULL (replacement may be NULL) */
  void
  (*post_remove)(struct bst *, struct bst_node *, struct bst_node *, int);
};

struct
bst
{
  struct bst_node *
  root;

  size_t
  count;

  struct bst_comparator
  comparator;

  struct bst_allocator
  allocator;

  struct bst_overrides
  overrides;
};


extern
char const * const
libbst_version;


int
bst_init(struct bst *t, struct bst_comparator const *comparator);

int
bst_inita(struct bst *t, struct bst_comparator const *comparator,
          struct bst_allocator const *allocator);

void
bst_destroy(struct bst *t, void (*datum_destroy)(void *, void *), void *arg);

# ifdef __GNUC__
__attribute__ ((pure))
# endif
size_t
bst_height(struct bst *t);

int
bst_insert(struct bst *t, void *datum);

int
bst_find(struct bst *t, void const *datum, void **store);

# ifdef __GNUC__
__attribute__ ((pure))
# endif
int
bst_contains(struct bst *t, void const *datum);

int
bst_remove(struct bst *t, void const *datum, void **store);

size_t
bst_preorder(struct bst *t, int (*visit)(void *, void *), void *arg);

size_t
bst_inorder(struct bst *t, int (*visit)(void *, void *), void *arg);

size_t
bst_postorder(struct bst *t, int (*visit)(void *, void *), void *arg);

/* O(n*h) time, O(1) space */
size_t
bst_levelorder(struct bst *t, int (*visit)(void *, void *), void *arg);

/* O(n) time, O(n) space */
size_t
bst_levelorder_fast(struct bst *t, int (*visit)(void *, void *), void *arg);


# ifdef __cplusplus
}
# endif


#endif
