/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "avl.h"

#include "bst.h"
#include "macros.h"
#include "rotations.h"


#define TO_AVL(n) ((struct avl_node *) (n))
#define TO_BST(n) ((struct bst_node *) (n))


static
void
_avl_assign_overrides(struct bst *t);

static
struct bst_node *
_avl_new_node(struct bst *t);

static
void
_avl_delete_node(struct bst *t, struct bst_node *node);

static
void
_avl_rebalance_insert(struct bst *t, struct bst_node *inserted_node);

static
void
_avl_rebalance_remove(struct bst *t, struct bst_node *removed,
                      struct bst_node *replacement, int was_left);


int
avl_init(struct bst *t, struct bst_comparator const *comparator)
{
  int ret;


  ret = bst_init(t, comparator);
  if (ret)
  {
    return ret;
  }

  _avl_assign_overrides(t);

  return 0;
}

int
avl_inita(struct bst *t, struct bst_comparator const *comparator,
          struct bst_allocator const *allocator)
{
  int ret;


  ret = bst_inita(t, comparator, allocator);
  if (ret)
  {
    return ret;
  }

  _avl_assign_overrides(t);

  return 0;
}


/*******************
 * local functions *
 *******************/

void
_avl_assign_overrides(struct bst *t)
{
  t->overrides.new_node = &_avl_new_node;
  t->overrides.delete_node = &_avl_delete_node;
  t->overrides.post_insert = &_avl_rebalance_insert;
  t->overrides.post_remove = &_avl_rebalance_remove;
}

struct bst_node *
_avl_new_node(struct bst *t)
{
  struct avl_node *new_node;


  new_node = (*t->allocator.allocate)(sizeof(*new_node), t->allocator.arg);
  if (NULL == new_node)
  {
    return NULL;
  }

  return TO_BST(new_node);
}

void
_avl_delete_node(struct bst *t, struct bst_node *node)
{
  struct avl_node *deleted_node;


  deleted_node = TO_AVL(node);

  (*t->allocator.deallocate)(sizeof(*deleted_node), deleted_node, t->allocator.arg);
}

void
_avl_rebalance_insert(struct bst *t, struct bst_node *inserted_node)
{
  struct avl_node *n, *p, *c;
  int done;


  n = TO_AVL(inserted_node);
  p = TO_AVL(n->bst_node.parent);

  n->weight = AVL_WEIGHT_NEUTRAL;

  done = 0;
  while (!done && NULL != p)
  {
    if (IS_LEFT(TO_BST(n)))
    {
      switch (p->weight)
      {
        case AVL_WEIGHT_LEFT_HEAVY:
          /*
           *      P
           *     / \
           *    N   -
           *   / \
           *  + ^ +
           */
          if (AVL_WEIGHT_RIGHT_HEAVY == n->weight)
          {
            /*
             *      P
             *     / \
             *    N   -
             *   / \
             *  -   +
             */
            c = TO_AVL(n->bst_node.right);
            /*
             *      P
             *     / \
             *    N   -
             *   / \
             *  -   C
             *     / \
             *    - | -
             */
            bst_node_rotate_left(TO_BST(c));
            bst_node_rotate_right(TO_BST(c));
            /*
             *       C
             *     /   \
             *    N     P
             *   / \   / \
             *  -   -|-   -
             */
            n->weight = (AVL_WEIGHT_RIGHT_HEAVY == c->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            p->weight = (AVL_WEIGHT_LEFT_HEAVY == c->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            c->weight = AVL_WEIGHT_NEUTRAL;

            if (TO_BST(p) == t->root)
            {
              t->root = TO_BST(c);
            }

            done = 1;
          }
          else
          {
            /*
             *      P
             *     / \
             *    N   -
             *   / \
             *  +   -
             */
            bst_node_rotate_right(TO_BST(n));
            /*
             *    N
             *   / \
             *  +   P
             *     / \
             *    -   -
             */
            n->weight = AVL_WEIGHT_NEUTRAL;
            p->weight = AVL_WEIGHT_NEUTRAL;

            if (TO_BST(p) == t->root)
            {
              t->root = TO_BST(n);
            }

            done = 1;
          }
          break;
        case AVL_WEIGHT_NEUTRAL:
          /*
           *      P
           *     / \
           *    N   -
           *   / \
           *  - | -
           */
          p->weight = AVL_WEIGHT_LEFT_HEAVY;
          break;
        case AVL_WEIGHT_RIGHT_HEAVY:
          /*
           *      P
           *     / \
           *    N   +
           *   / \
           *  - | -
           */
          p->weight = AVL_WEIGHT_NEUTRAL;
          done = 1;
          break;
        default:
          /* won't happen */
          break;
      }
    }
    else
    {
      switch (p->weight)
      {
        case AVL_WEIGHT_RIGHT_HEAVY:
          if (AVL_WEIGHT_LEFT_HEAVY == n->weight)
          {
            c = TO_AVL(n->bst_node.left);

            bst_node_rotate_right(TO_BST(c));
            bst_node_rotate_left(TO_BST(c));

            n->weight = (AVL_WEIGHT_LEFT_HEAVY == c->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            p->weight = (AVL_WEIGHT_RIGHT_HEAVY == c->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            c->weight = AVL_WEIGHT_NEUTRAL;

            if (TO_BST(p) == t->root)
            {
              t->root = TO_BST(c);
            }

            done = 1;
          }
          else
          {
            bst_node_rotate_left(TO_BST(n));

            n->weight = AVL_WEIGHT_NEUTRAL;
            p->weight = AVL_WEIGHT_NEUTRAL;

            if (TO_BST(p) == t->root)
            {
              t->root = TO_BST(n);
            }

            done = 1;
          }
          break;
        case AVL_WEIGHT_NEUTRAL:
          p->weight = AVL_WEIGHT_RIGHT_HEAVY;
          break;
        case AVL_WEIGHT_LEFT_HEAVY:
          p->weight = AVL_WEIGHT_NEUTRAL;
          done = 1;
          break;
        default:
          /* won't happen */
          break;
      }
    }

    n = p;
    p = TO_AVL(n->bst_node.parent);
  }
}

void
_avl_rebalance_remove(struct bst *t, struct bst_node *removed,
                      struct bst_node *replacement, int was_left)
{
  struct avl_node *n, *p, *s, *sc;
  int done, first, flag;


  n = TO_AVL(replacement);
  p = TO_AVL(removed->parent);

  first = 1;
  done = 0;
  while (!done && NULL != p)
  {
    if ((first && was_left) || (!first && IS_LEFT(TO_BST(n))))
    {
      switch (p->weight)
      {
        case AVL_WEIGHT_LEFT_HEAVY:
          /*
           * was:
           *      P
           *     / \
           *    N   -
           *   / \
           *  - | -
           */
          /*
           * now:
           *      P
           *     / \
           *    N   -
           *   / \
           *  --|--
           */
          p->weight = AVL_WEIGHT_NEUTRAL;
          /* height of subtree still has changed; not done */
          break;
        case AVL_WEIGHT_NEUTRAL:
          /*
           * was:
           *      P
           *     / \
           *    N   +
           *   / \
           *  - | -
           */
          /*
           * now:
           *      P
           *     / \
           *    N   +
           *   / \
           *  --|--
           */
          p->weight = AVL_WEIGHT_RIGHT_HEAVY;
          /* height of subtree has not changed */
          done = 1;
          break;
        case AVL_WEIGHT_RIGHT_HEAVY:
          /*
           * was:
           *      P
           *     / \
           *    N  ++
           *   / \
           *  - | -
           *
           */
          /*
           * now:
           *      P
           *     / \
           *    N  ++
           *   / \
           *  --|--
           */
          s = TO_AVL(p->bst_node.right);
          /*
           *       P
           *     /   \
           *    N     S
           *   / \   / \
           *  --|-- + | +
           */
          if (AVL_WEIGHT_LEFT_HEAVY == s->weight)
          {
            /*
             *       P
             *     /   \
             *    N     S
             *   / \   / \
             *  --|-- +   -
             */
            sc = TO_AVL(s->bst_node.left);
            /*
             *        P
             *     /     \
             *    N       S
             *   / \     / \
             *  --|--   SC  -
             *         / \
             *        - | -
             */
            bst_node_rotate_right(TO_BST(sc));
            bst_node_rotate_left(TO_BST(sc));
            /*
             *         SC
             *       /   \
             *      P     S
             *     / \   / \
             *    N   -|-   -
             *   / \
             *  --|--
             */
            p->weight = (AVL_WEIGHT_RIGHT_HEAVY == sc->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = (AVL_WEIGHT_LEFT_HEAVY == sc->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            sc->weight = AVL_WEIGHT_NEUTRAL;

            if (TO_BST(p) == t->root)
            {
              t->root = TO_BST(sc);
            }

            /* height of subtree has still changed; not done */
            /* set n and p so next iteration uses correct nodes */
            n = p;
            p = sc;
          }
          else
          {
            /*
             *        P
             *     /     \
             *    N       S
             *   / \     / \
             *  --|--  +/-  +
             */
            bst_node_rotate_left(TO_BST(s));
            /*
             *        S
             *       / \
             *      P   +
             *     / \
             *    N  +/-
             *   / \
             *  --|--
             */
            flag = AVL_WEIGHT_NEUTRAL == s->weight;
            p->weight = (flag)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = (flag)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;

            if (TO_BST(p) == t->root)
            {
              t->root = TO_BST(s);
            }

            /*
             * if S was neutral, height hasn't changed, but if it was
             * right-heavy, it's still shorter
             */
            done = flag;
            /* set n and p so next (possible) iteration uses correct nodes */
            n = p;
            p = s;
          }
          break;
        default:
          /* won't happen */
          break;
      }
    }
    else
    {
      switch (p->weight)
      {
        case AVL_WEIGHT_RIGHT_HEAVY:
          p->weight = AVL_WEIGHT_NEUTRAL;
          break;
        case AVL_WEIGHT_NEUTRAL:
          p->weight = AVL_WEIGHT_LEFT_HEAVY;
          done = 1;
          break;
        case AVL_WEIGHT_LEFT_HEAVY:
          s = TO_AVL(p->bst_node.left);

          if (AVL_WEIGHT_RIGHT_HEAVY == s->weight)
          {
            sc = TO_AVL(s->bst_node.right);

            bst_node_rotate_left(TO_BST(sc));
            bst_node_rotate_right(TO_BST(sc));

            p->weight = (AVL_WEIGHT_LEFT_HEAVY == sc->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = (AVL_WEIGHT_RIGHT_HEAVY == sc->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            sc->weight = AVL_WEIGHT_NEUTRAL;

            if (TO_BST(p) == t->root)
            {
              t->root = TO_BST(sc);
            }

            n = p;
            p = sc;
          }
          else
          {
            bst_node_rotate_right(TO_BST(s));

            flag = AVL_WEIGHT_NEUTRAL == s->weight;
            p->weight = (flag)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = (flag)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;

            if (TO_BST(p) == t->root)
            {
              t->root = TO_BST(s);
            }

            done = flag;

            n = p;
            p = s;
          }
          break;
        default:
          /* won't happen */
          break;
      }
    }

    first = 0;

    n = p;
    p = TO_AVL(n->bst_node.parent);
  }
}
