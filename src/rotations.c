/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "rotations.h"

#include "bst.h"
#include "macros.h"


void
bst_node_rotate_left(struct bst_node *n)
{
  struct bst_node *c, *p;


  c = n->left;
  p = n->parent;

  if (HAS_PARENT(p))
  {
    if (IS_LEFT(p))
    {
      p->parent->left = n;
    }
    else
    {
      p->parent->right = n;
    }
  }
  n->parent = p->parent;

  n->left = p;
  p->parent = n;

  p->right = c;
  if (NULL != c)
  {
    c->parent = p;
  }
}

void
bst_node_rotate_right(struct bst_node *n)
{
  struct bst_node *c, *p;


  c = n->right;
  p = n->parent;

  if (HAS_PARENT(p))
  {
    if (IS_LEFT(p))
    {
      p->parent->left = n;
    }
    else
    {
      p->parent->right = n;
    }
  }
  n->parent = p->parent;

  n->right = p;
  p->parent = n;

  p->left = c;
  if (NULL != c)
  {
    c->parent = p;
  }
}
