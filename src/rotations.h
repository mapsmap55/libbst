/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef LIBBST_ROTATIONS_H
# define LIBBST_ROTATIONS_H


# include "bst.h"


# ifdef __cplusplus
extern "C"
{
# endif


/*
 *  G ^ G        G ^ G
 *   \ /          \ /
 *    P            N
 *   / \    ->    / \
 *  X   N        P   Y
 *     / \      / \
 *    C   Y    X   C
 */
void
bst_node_rotate_left(struct bst_node *n);

/*
 *    G ^ G    G ^ G
 *     \ /      \ /
 *      P        N
 *     / \  ->  / \
 *    N   Y    X   P
 *   / \          / \
 *  X   C        C   Y
 */
void
bst_node_rotate_right(struct bst_node *n);


# ifdef __cplusplus
}
# endif


#endif
