/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of libbst.
 *
 * Libbst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Libbst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbst.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "bst.h"

#include <stdlib.h>

#ifdef HAVE_CONFIG_H
# include <config.h>
#else
# define PACKAGE_VERSION ("0.0.0")
#endif

#include "macros.h"


enum
bst_traverse_state
{
  BST_TRAVERSE_STATE_PRE,
  BST_TRAVERSE_STATE_IN,
  BST_TRAVERSE_STATE_POST
};

struct
bst_traverse_destroy_args
{
  void
  (*datum_destroy)(void *, void *);

  void *
  arg;

  struct bst *
  tree;
};

struct
bst_traverse_visit_wrapper_args
{
  int
  (*visit)(void *, void *);

  void *
  arg;

  size_t
  count;
};

struct
bst_node_queue
{
  struct bst_node **
  data;

  size_t
  data_len;

  size_t
  front;

  size_t
  back;

  int
  data_on_stack;
};


char const * const
libbst_version
  = PACKAGE_VERSION;


static
void *
_bst_default_allocate(size_t size, void *arg);

static
void
_bst_default_deallocate(size_t size, void *ptr, void *arg);

static
struct bst_node *
_bst_new_node(struct bst *t);

static
void
_bst_delete_node(struct bst *t, struct bst_node *node);

static
void
_bst_traverse(struct bst *t, enum bst_traverse_state visit_state,
              int (*visit)(struct bst_node *, void *), void *arg);

static
int
_bst_traverse_destroy(struct bst_node *node, void *arg);

static
int
_bst_traverse_visit_wrapper(struct bst_node *node, void *arg);

static
#ifdef __GNUC__
__attribute__ ((pure))
#endif
size_t
_bst_height(struct bst_node *root);

static
struct bst_node *
_bst_find_last(struct bst *t, void const *datum, int *last_cmp);

static
int
_bst_node_queue_enqueue(struct bst_node_queue *q, struct bst_node *node);

static
int
_bst_node_queue_dequeue(struct bst_node_queue *q, struct bst_node **store);


int
bst_init(struct bst *t, struct bst_comparator const *comparator)
{
  struct bst_allocator default_allocator;


  default_allocator.allocate = &_bst_default_allocate;
  default_allocator.deallocate = &_bst_default_deallocate;
  default_allocator.arg = NULL;

  return bst_inita(t, comparator, &default_allocator);
}

int
bst_inita(struct bst *t, struct bst_comparator const *comparator,
          struct bst_allocator const *allocator)
{
  t->root = NULL;
  t->count = 0;
  t->comparator = *comparator;
  t->allocator = *allocator;
  t->overrides.new_node = &_bst_new_node;
  t->overrides.delete_node = &_bst_delete_node;
  t->overrides.post_insert = NULL;
  t->overrides.post_find = NULL;
  t->overrides.post_remove = NULL;

  return NULL == t->comparator.compare || NULL == t->allocator.allocate
           || NULL == t->allocator.deallocate;
}

void
bst_destroy(struct bst *t, void (*datum_destroy)(void *, void *), void *arg)
{
  struct bst_traverse_destroy_args args;


  args.datum_destroy = datum_destroy;
  args.arg = arg;
  args.tree = t;
  _bst_traverse(t, BST_TRAVERSE_STATE_POST, &_bst_traverse_destroy, &args);
}

size_t
bst_height(struct bst *t)
{
  return _bst_height(t->root);
}

int
bst_insert(struct bst *t, void *datum)
{
  struct bst_node *found, *new_node;
  int last_cmp;


  found = _bst_find_last(t, datum, &last_cmp);
  if (NULL != found && 0 == last_cmp)
  {
    if (NULL != found && NULL != t->overrides.post_find)
    {
      (*t->overrides.post_find)(t, found, 0);
    }

    return 1;
  }

  new_node = (*t->overrides.new_node)(t);
  if (NULL == new_node)
  {
    if (NULL != found && NULL != t->overrides.post_find)
    {
      (*t->overrides.post_find)(t, found, 0);
    }

    return 2;
  }

  new_node->datum = datum;
  new_node->parent = found;
  new_node->left = NULL;
  new_node->right = NULL;

  if (NULL == found)
  {
    t->root = new_node;
  }
  else
  {
    if (0 > last_cmp)
    {
      found->left = new_node;
    }
    else
    {
      found->right = new_node;
    }
  }

  ++t->count;

  if (NULL != t->overrides.post_insert)
  {
    (*t->overrides.post_insert)(t, new_node);
  }

  return 0;
}

int
bst_find(struct bst *t, void const *datum, void **store)
{
  struct bst_node *found;
  int last_cmp;


  found = _bst_find_last(t, datum, &last_cmp);
  if (NULL == found || 0 != last_cmp)
  {
    if (NULL != found && NULL != t->overrides.post_find)
    {
      (*t->overrides.post_find)(t, found, 0);
    }

    return 1;
  }

  if (NULL != store)
  {
    *store = found->datum;
  }

  if (NULL != t->overrides.post_find)
  {
    (*t->overrides.post_find)(t, found, 1);
  }

  return 0;
}

int
bst_contains(struct bst *t, void const *datum)
{
  return !bst_find(t, datum, NULL);
}

int
bst_remove(struct bst *t, void const *datum, void **store)
{
  struct bst_node *found, *tmp;
  int last_cmp, was_left;


  found = _bst_find_last(t, datum, &last_cmp);
  if (NULL == found || 0 != last_cmp)
  {
    if (NULL != found && NULL != t->overrides.post_find)
    {
      (*t->overrides.post_find)(t, found, 0);
    }

    return 1;
  }

  if (NULL != store)
  {
    *store = found->datum;
  }

  if (IS_INNER(found))
  {
    tmp = found->right;
    while (HAS_LEFT(tmp))
    {
      tmp = tmp->left;
    }
    found->datum = tmp->datum;
    found = tmp;
  }

  tmp = (HAS_LEFT(found)) ? found->left : found->right;
  was_left = IS_LEFT(found);

  if (!HAS_PARENT(found))
  {
    t->root = tmp;
  }
  else
  {
    if (IS_LEFT(found))
    {
      found->parent->left = tmp;
    }
    else
    {
      found->parent->right = tmp;
    }
  }
  if (NULL != tmp)
  {
    tmp->parent = found->parent;
  }

  --t->count;

  if (NULL != t->overrides.post_remove)
  {
    (*t->overrides.post_remove)(t, found, tmp, was_left);
  }

  (*t->overrides.delete_node)(t, found);

  return 0;
}

size_t
bst_preorder(struct bst *t, int (*visit)(void *, void *), void *arg)
{
  struct bst_traverse_visit_wrapper_args args;


  args.visit = visit;
  args.arg = arg;
  args.count = 0;
  _bst_traverse(t, BST_TRAVERSE_STATE_PRE, &_bst_traverse_visit_wrapper, &args);

  return args.count;
}

size_t
bst_inorder(struct bst *t, int (*visit)(void *, void *), void *arg)
{
  struct bst_traverse_visit_wrapper_args args;


  args.visit = visit;
  args.arg = arg;
  args.count = 0;
  _bst_traverse(t, BST_TRAVERSE_STATE_IN, &_bst_traverse_visit_wrapper, &args);

  return args.count;
}

size_t
bst_postorder(struct bst *t, int (*visit)(void *, void *), void *arg)
{
  struct bst_traverse_visit_wrapper_args args;


  args.visit = visit;
  args.arg = arg;
  args.count = 0;
  _bst_traverse(t, BST_TRAVERSE_STATE_POST, &_bst_traverse_visit_wrapper,
                &args);

  return args.count;
}

size_t
bst_levelorder(struct bst *t, int (*visit)(void *, void *), void *arg)
{
  struct bst_node *curr_node, *next_node;
  enum bst_traverse_state curr_state, next_state;
  size_t count, depth, target_depth;
  int depth_successful;


  if (NULL == t->root)
  {
    return 0;
  }

  count = 0;

  target_depth = 0;
  depth_successful = !(*visit)(t->root->datum, arg);
  while (depth_successful)
  {
    ++target_depth;
    depth_successful = 0;

    depth = 0;
    curr_node = t->root;
    curr_state = BST_TRAVERSE_STATE_PRE;
    while (NULL != curr_node)
    {
      switch (curr_state)
      {
        case BST_TRAVERSE_STATE_PRE:
          if (HAS_LEFT(curr_node))
          {
            next_node = curr_node->left;
            next_state = BST_TRAVERSE_STATE_PRE;
            ++depth;
          }
          else
          {
            next_node = curr_node;
            next_state = BST_TRAVERSE_STATE_IN;
          }
          break;
        case BST_TRAVERSE_STATE_IN:
          if (HAS_RIGHT(curr_node))
          {
            next_node = curr_node->right;
            next_state = BST_TRAVERSE_STATE_PRE;
            ++depth;
          }
          else
          {
            next_node = curr_node;
            next_state = BST_TRAVERSE_STATE_POST;
          }
          break;
        case BST_TRAVERSE_STATE_POST:
          next_state = (IS_LEFT(curr_node))
                         ? BST_TRAVERSE_STATE_IN
                         : BST_TRAVERSE_STATE_POST;
          next_node = curr_node->parent;
          --depth;
          break;
      }

      if (depth == target_depth)
      {
        ++count;
        depth_successful = 1;

        if ((*visit)(next_node->datum, arg))
        {
          return count;
        }

        next_state = (IS_LEFT(next_node))
                       ? BST_TRAVERSE_STATE_IN
                       : BST_TRAVERSE_STATE_POST;
        next_node = next_node->parent;
        --depth;
      }

      curr_node = next_node;
      curr_state = next_state;
    }
  }

  return count;
}

size_t
bst_levelorder_fast(struct bst *t, int (*visit)(void *, void *), void *arg)
{
  struct bst_node_queue q;
  struct bst_node *curr, *small_data[16];
  size_t count;


  if (NULL == t->root)
  {
    return 0;
  }

  q.data_len = sizeof(small_data) / sizeof(*small_data);
  q.data = small_data;
  q.front = 0;
  q.back = 0;
  q.data_on_stack = 1;

  count = 0;

  _bst_node_queue_enqueue(&q, t->root);
  while (q.front != q.back)
  {
    _bst_node_queue_dequeue(&q, &curr);

    ++count;
    if ((*visit)(curr->datum, arg))
    {
      break;
    }

    if (HAS_LEFT(curr))
    {
      if (_bst_node_queue_enqueue(&q, curr->left))
      {
        break;
      }
    }
    if (HAS_RIGHT(curr))
    {
      if (_bst_node_queue_enqueue(&q, curr->right))
      {
        break;
      }
    }
  }

  if (!q.data_on_stack)
  {
    free(q.data);
  }

  return count;
}


/*******************
 * local functions *
 *******************/

void *
_bst_default_allocate(size_t size, void *arg)
{
  (void) arg;

  return malloc(size);
}

void
_bst_default_deallocate(size_t size, void *ptr, void *arg)
{
  (void) size;
  (void) arg;

  free(ptr);
}

struct bst_node *
_bst_new_node(struct bst *t)
{
  struct bst_node *new_node;


  new_node = (*t->allocator.allocate)(sizeof(*new_node), t->allocator.arg);

  return new_node;
}

void
_bst_delete_node(struct bst *t, struct bst_node *node)
{
  (*t->allocator.deallocate)(sizeof(*node), node, t->allocator.arg);
}

void
_bst_traverse(struct bst *t, enum bst_traverse_state visit_state,
              int (*visit)(struct bst_node *, void *), void *arg)
{
  struct bst_node *curr_node, *next_node;
  enum bst_traverse_state curr_state, next_state;


  curr_node = t->root;
  curr_state = BST_TRAVERSE_STATE_PRE;
  while (NULL != curr_node)
  {
    switch (curr_state)
    {
      case BST_TRAVERSE_STATE_PRE:
        if (HAS_LEFT(curr_node))
        {
          next_node = curr_node->left;
          next_state = BST_TRAVERSE_STATE_PRE;
        }
        else
        {
          next_node = curr_node;
          next_state = BST_TRAVERSE_STATE_IN;
        }
        break;
      case BST_TRAVERSE_STATE_IN:
        if (HAS_RIGHT(curr_node))
        {
          next_node = curr_node->right;
          next_state = BST_TRAVERSE_STATE_PRE;
        }
        else
        {
          next_node = curr_node;
          next_state = BST_TRAVERSE_STATE_POST;
        }
        break;
      case BST_TRAVERSE_STATE_POST:
        next_state = (IS_LEFT(curr_node))
                       ? BST_TRAVERSE_STATE_IN
                       : BST_TRAVERSE_STATE_POST;
        next_node = curr_node->parent;
        break;
    }

    if (curr_state == visit_state && (*visit)(curr_node, arg))
    {
      break;
    }

    curr_node = next_node;
    curr_state = next_state;
  }
}

int
_bst_traverse_destroy(struct bst_node *node, void *arg)
{
  struct bst_traverse_destroy_args const *args;


  args = arg;

  if (NULL != args->datum_destroy)
  {
    (*args->datum_destroy)(node->datum, args->arg);
  }

  (*args->tree->overrides.delete_node)(args->tree, node);

  return 0;
}

int
_bst_traverse_visit_wrapper(struct bst_node *node, void *arg)
{
  struct bst_traverse_visit_wrapper_args *args;


  args = arg;

  ++args->count;

  return (*args->visit)(node->datum, args->arg);
}

size_t
_bst_height(struct bst_node *root)
{
  size_t l, r;


  if (NULL == root)
  {
    return 0;
  }

  l = _bst_height(root->left);
  r = _bst_height(root->right);

  return 1 + ((l > r) ? l : r);
}

struct bst_node *
_bst_find_last(struct bst *t, void const *datum, int *last_cmp)
{
  struct bst_node *curr, *last;
  int c;


  last = NULL;
  curr = t->root;
  c = 0;

  while (NULL != curr)
  {
    last = curr;

    c = (*t->comparator.compare)(datum, curr->datum, t->comparator.arg);
    if (0 == c)
    {
      break;
    }

    if (0 > c)
    {
      curr = curr->left;
    }
    else
    {
      curr = curr->right;
    }
  }

  *last_cmp = c;

  return (NULL != curr) ? curr : last;
}

int
_bst_node_queue_enqueue(struct bst_node_queue *q, struct bst_node *node)
{
  struct bst_node **new_data;
  size_t queue_size, new_data_len, i;


  queue_size = (q->front <= q->back)
                 ? q->back - q->front
                 : q->data_len + q->back - q->front;

  if (q->data_len == queue_size + 1)
  {
    new_data_len = 2 * q->data_len;

    if (!q->data_on_stack)
    {
      new_data = realloc(q->data, new_data_len * sizeof(*q->data));
      if (NULL == new_data)
      {
        return 1;
      }

      if (q->back < q->front)
      {
        for (i = 0; i < q->back; ++i)
        {
          new_data[q->data_len + i] = new_data[i];
        }

        q->back += q->data_len;
      }
    }
    else
    {
      new_data = malloc(new_data_len * sizeof(*q->data));
      if (NULL == new_data)
      {
        return 1;
      }

      if (q->front <= q->back)
      {
        for (i = q->front; i < q->back; ++i)
        {
          new_data[i] = q->data[i];
        }
      }
      else
      {
        for (i = q->front; i < q->data_len; ++i)
        {
          new_data[i] = q->data[i];
        }
        for (i = 0; i < q->back; ++i)
        {
          new_data[q->data_len + i] = q->data[i];
        }

        q->back += q->data_len;
      }

      q->data_on_stack = 0;
    }

    q->data = new_data;
    q->data_len = new_data_len;
  }

  q->data[q->back] = node;
  q->back = (q->back + 1) % q->data_len;

  return 0;
}

int
_bst_node_queue_dequeue(struct bst_node_queue *q, struct bst_node **store)
{
  if (q->front == q->back)
  {
    return 1;
  }

  *store = q->data[q->front];
  q->front = (q->front + 1) % q->data_len;

  return 0;
}
